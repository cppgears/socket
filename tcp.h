#pragma once
#include "socket.h"
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <functional>
#include <list>
#include <vector>
#include <csignal>
#include <mutex>

namespace gear {
	namespace socket {
		namespace tcp {
			using namespace std;
			class client : public handle {
			protected:
				friend class server;
				client(int sock, address& addr) { attach(sock, move(addr)); }

			public:
				client() { ; }
				client(const client& c) {
					attach(c.h(), move(((client&&)c).addr()));
				}
				client(const client&& c) {
					attach(c.h(), move(((client&&)c).addr()));
				}
				client& operator = (const client&) = delete;
				client& operator = (const client&& c) {
					attach(c.h(), move(((client&&)c).addr()));
					return *this;
				}

				inline bool connect(string host, string port,int timeout = 10) {
					if (!s_handle) {
						signal(SIGPIPE, SIG_IGN);

						addrinfo hinfo(host, port, SOCK_STREAM);
						int s = -1;
						for (auto res = hinfo.get(); res != nullptr; res = res->ai_next) {
							s = ::socket(res->ai_family, res->ai_socktype, res->ai_protocol);

							if (s == -1) {
								continue;
							}

							if (::connect(s, res->ai_addr, (int)res->ai_addrlen)) {
								if (errno == EINPROGRESS ||
									errno == EAGAIN ||
									errno == EWOULDBLOCK)
								{
									pollfd fd;
									fd.fd = s;
									fd.events = POLLOUT;
									auto&& rval = (int)::poll(&fd, 1, timeout);

									if (rval > 0) {
										int opt;
										socklen_t len = sizeof(opt);
										getsockopt(s, SOL_SOCKET, SO_ERROR, (char*)&opt, &len);
										if (opt) {
											::close(s);
											s = -1;
											__error(opt);
											return false;
										}
										/* success */
										break;
									}
								}
								::close(s);
								s = -1;
							}
							else {
								break;
							}
						}
						if (s <= 0) {
							__error(ENOTCONN);
							return false;
						}
						s_handle = s;
					}
					return true;
				}
				/*
				inline bool connect() {
					return_ifnotsock(s_handle);
					if (int error = __getsockerror()) {
						__error(error);
						return false;
					}
					if (::connect(s_handle, &s_addr.sa(), sizeof(sockaddr_in)) == -1) {
						if (errno != EISCONN) {
							__error(errno);
							return false;
						}
					}
					return true;
				}
				*/
				inline void disconnect() {
					::shutdown(s_handle, SHUT_RDWR);
					close();
				}

				inline ssize_t poll(short events, int timeout) noexcept {
					pollfd fd;
					fd.fd = s_handle;
					fd.events = events;
					ssize_t rval = ::poll(&fd, 1, timeout);

					if (rval > 0) {
						int opt;
						socklen_t len = sizeof(opt);
						getsockopt(s_handle, SOL_SOCKET, SO_ERROR, (char*)&opt, &len);
						if (opt) {
							return -1;
						}
						return 0;
					}

					return 1;
				}

				inline ssize_t recv(void* buffer, size_t length, int flags = 0) {
					return_ifnotsock(s_handle);
					return ::recv(s_handle, buffer, (int)length, flags);
				}

				inline bool wait(function<void(client&& sock)>&& fn_data, double timeout_millisec = 5000) {
					return_ifnotsock(s_handle);

					fd_set read, except;
					struct timeval t = { (time_t)(floor((int)(timeout_millisec * 1000.)) / 1000000) , ((time_t)(timeout_millisec * 1000.)) % 1000000 };

					FD_ZERO(&read); FD_ZERO(&except);
					FD_SET(s_handle, &read); FD_SET(s_handle, &except);

					switch (::select(s_handle + 1, &read, nullptr, &except, &t)) {
					case -1:
						if (errno != EAGAIN) {
							__error(errno);
							return false;
						}
					case 0:
						break;
					default:
					{
						if (FD_ISSET(s_handle, &read)) {
							char data;
							int result = (int)::recv(s_handle, &data, (int)1, MSG_PEEK);
							if (result > 0) {
								fn_data(move(*this));
							}
							else if (result < 0) {
								__error(errno);
								return false;
							}
							else {
								__error(EPIPE);
								return false;
							}
						}
						if (FD_ISSET(s_handle, &except)) {
							if (int error = __getsockerror()) {
								__error(error);
								return false;
							}
						}
					}
					}
					return true;
				}

				inline ssize_t send(const void* buffer, int length, int flags = 0) {
					return_ifnotsock(s_handle);
					return ::send(s_handle, buffer, (int)length, flags);
				}

				inline bool setkeepalive(bool enable = true, int maxpkt = 10) {
					return_ifnotsock(s_handle);
					return (!setopt(SOL_SOCKET, SO_KEEPALIVE, (int)enable) &&
						!setopt(IPPROTO_TCP, TCP_KEEPIDLE, (int)enable) &&
						!setopt(IPPROTO_TCP, TCP_KEEPINTVL, (int)enable) &&
						!setopt(IPPROTO_TCP, TCP_KEEPCNT, maxpkt));
				}

				inline bool setnodelay(bool enable = true) {
					return_ifnotsock(s_handle);
					return !setopt(IPPROTO_TCP, TCP_NODELAY, (int)enable);
				}
			};

			class server : public handle
			{
			protected:
				class events_list {
				protected:
					friend class server;
					function<bool(int, address&)> fn_accept;
					function<void(int)> fn_receive;
					function<void(int)> fn_send;
					function<void(int)> fn_close;
					function<void(int,int)> fn_except;
					events_list() { ; }

					inline bool on_connect(int so, address&& a) { if (fn_accept) { return fn_accept(so, a); } return true; }
					inline void on_receive(int so) { if (fn_receive) { fn_receive(so); } }
					inline void on_send(int so) { if (fn_send) { fn_send(so); } }
					inline void on_close(int so) { if (fn_close) { fn_close(so); } }
					inline void on_exept(int so, int err) { if (fn_except) { fn_except(so, err); } }
				public:
					inline events_list& connect(function<bool(int, address&)>&& fn) { fn_accept = fn; return *this; }
					inline events_list& receive(function<void(int)>&& fn) { fn_receive = fn; return *this; }
					inline events_list& disconnect(function<void(int)>&& fn) { fn_close = fn; return *this; }
					inline events_list& except(function<void(int,int)>&& fn) { fn_except = fn; return *this; }
				};
			private:
				vector<struct pollfd>	clients;
				size_t					nclients;
				mutex					lock;
				list<int>				leave;
				bool					compress;

				inline void vaccuum() {
					compress = false;
					for (size_t i = 1; i < nclients; i++)
					{
						if (clients[i].fd == -1)
						{
							for (size_t j = i; j < nclients; j++)
							{
								clients[j].fd = clients[j + 1].fd;
							}
							i--;
							nclients--;
						}
					}
				}

			public:
				events_list	events;

				server() : nclients(0){ ; }
				~server() { ; }

				/*
				* Disconnect all clients
				*/
				inline void disconnect() {
					
					for (size_t i = 1; i < nclients; i++)
					{
						if (clients[i].fd >= 0) {
							//::shutdown(clients[i].fd, SHUT_RDWR);
							::close(clients[i].fd);
							events.on_close(clients[i].fd);
							clients[i].fd = 0;
						}
					}
					nclients = 1;
				}

				inline void disconnect(int c) {
					leave.push_back(c);
					events.on_close(c);
					/*
					for (size_t i = 1; i < nclients; i++)
					{
						if (clients[i].fd == c) {
							::shutdown(clients[i].fd, SHUT_RDWR);
							::close(clients[i].fd);
							events.on_close(clients[i].fd);
							clients[i].fd = -1;
							break;
						}
					}
					*/
				}

				/*
				* Open tcp server
				*/
				inline bool open(uint16_t port, string ipaddr = string(), bool fast_open = false, int maxconn = SOMAXCONN, bool reuse_address = true) {

					disconnect();
					close();

					if ((s_handle = ::socket(AF_INET, SOCK_STREAM | SOCK_NONBLOCK, IPPROTO_TCP)) <= 0) {
						__error(errno);
						return false;
					}

					fast_open && setopt(SOL_TCP, TCP_FASTOPEN, 5);

					reuse_address && setreuseaddr(1);

					s_addr.af_inet(port, ipaddr.empty() ? INADDR_ANY : inet_addr(ipaddr.c_str()));

					if (::bind(s_handle, &s_addr.sa(), sizeof(sockaddr_in)) == -1) {
						__error(errno);
						return false;
					}

					if (::listen(s_handle, maxconn) == -1) {
						__error(errno);
						close();
						return false;
					}
					clients.resize(maxconn + 1, { 0,0,0 });
					clients[0].fd = s_handle;
					clients[0].events = POLLIN;
					nclients = 1;

					return true;
				}

				/*
				* Listen client connection, send, recv, disconnect events
				*/
				inline bool listen(double timeout_millisec = 5000) {

					if (!leave.empty() || compress) {
						while(!leave.empty()) {
							auto s = leave.front();
							for (size_t n = 1; n < nclients; n++) {
								if (clients[n].fd == s) {
									//::shutdown(clients[n].fd, SHUT_RDWR);
									::close(clients[n].fd);
									clients[n].fd = -1;
									break;
								}
							}
							leave.pop_front();
						}
						vaccuum();
					}
					

					switch (::poll(clients.data(), nclients, (int)timeout_millisec)) {
					case -1:
						if (errno != EAGAIN) {
							__error(errno);
							return false;
						}
					case 0:
						return true;
					default:
					{
						for (size_t n = 0; n < nclients;n++) {
							
							auto&& fd = clients[n];

							if (fd.revents == 0)
								continue;
							
							if (fd.revents != POLLIN)
							{
								events.on_exept(fd.fd,errno);
								if (fd.fd == s_handle)
									return false;
								else {
									::close(fd.fd);
									events.on_close(fd.fd);
									fd.fd = -1;
									compress = true;
									break;
								}
							}
							if (fd.fd == s_handle) {
								/* accept connection  */
								int new_sd = -1;
								do {
									address addr;
									socklen_t len = sizeof(sockaddr_storage);
									new_sd = ::accept(s_handle, &addr.sa(), &len);
									if (new_sd < 0) {
										if (errno != EWOULDBLOCK)
										{
											events.on_exept(s_handle, errno);
										}
										break;
									}

									if (events.on_connect(new_sd, move(addr))) {
										if (nclients + 1 >= clients.size()) {
											clients.resize(nclients + 10, { 0,0,0 });
										}
										clients[nclients].fd = new_sd;
										clients[nclients].events = POLLIN;
										nclients++;
									}
									else {
										::close(new_sd);
										events.on_close(new_sd);
										break;
									}
								} while (new_sd > 0);
							}
							else if(fd.fd > 0) {
								/* client */
								uint8_t health;
								ssize_t rc = recv(fd.fd, &health, sizeof(health), MSG_PEEK);
								if(rc < 0) {
									if (errno != EWOULDBLOCK)
									{
										::close(fd.fd);
										events.on_exept(fd.fd,errno);
										events.on_close(fd.fd);
										fd.fd = -1;
										compress = true;
									}
									break;
								}
								else if (rc == 0)
								{
									::close(fd.fd);
									events.on_close(fd.fd);
									fd.fd = -1;
									compress = true;
									break;
								}
								else {
									events.on_receive(fd.fd);
								}
							}
						}
					}
					}
					return true;
				}
			};
		}
	}
}