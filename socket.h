#pragma once
#include <string>
#include <array>
#include <unordered_set>
#include <cstring>
#include <cmath>
#include <arpa/inet.h>
#include <unistd.h>
#include <execinfo.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <poll.h>
#include <cerrno>
#include <netdb.h>


#define return_ifnotsock_r(h,r) if (h == 0) {__error(ENOTSOCK); return r; }
#define return_ifnotsock(h) return_ifnotsock_r(h,false)
#define return_ifsockerror(h) if (h == -1) {__error(errno); return false; }
#define return_closes_ifsockerror(h) if (h == -1) {__error(errno); close(); return false; }


namespace gear {
		namespace socket {
		using namespace std;

		class exception {
		public:
			int			code;
			std::string message;

			exception(int err_no, const char* err_msg) : code(err_no), message(err_msg) { ; }
		};

		class addrinfo {
			struct ::addrinfo* _addrinfo;
			static inline bool is_localhost(const string&& host) {
				return
					host == "localhost" || host == "localhost.localdomain" ||
					host == "localhost6" || host == "localhost6.localdomain6" ||
					host == "::1" || host == "127.0.0.1";
			}
		public:
			addrinfo(const string& host, const string& port, int type) : _addrinfo(nullptr) {
				struct ::addrinfo hints;

				memset(&hints, 0, sizeof(hints));

				hints.ai_family = PF_UNSPEC;
				hints.ai_socktype = type;

				if (is_localhost(std::move(host))) {
					hints.ai_flags |= AI_ADDRCONFIG;
				}

				int error = getaddrinfo(host.c_str(), port.c_str(), &hints, &_addrinfo);

				if (error) {
					throw exception(error, gai_strerror(error));
				}
			}
			~addrinfo() {
				if (_addrinfo) {
					freeaddrinfo(_addrinfo);
				}
			}
			inline const ::addrinfo* get() const { return _addrinfo; }
		};

		class address : sockaddr_storage {
		private:
			template<typename T> T& cast() const {
				return *((T*)this);
			}
		public:
			address() { reset(); }

			address(const address&) = delete;
			address(const address&& a) { bcopy((sockaddr_storage*)&a, (sockaddr_storage*)this, sizeof(sockaddr_storage)); }

			address& operator = (const address&) = delete;
			address& operator = (const address&& a) { bcopy((sockaddr_storage*)&a, (sockaddr_storage*)this, sizeof(sockaddr_storage)); return *this; }

			address& swap(address&& a) {
				bcopy((sockaddr_storage*)&a, (sockaddr_storage*)this, sizeof(sockaddr_storage)); return *this;
			}


			inline void reset() { bzero(this, sizeof(sockaddr_storage)); }
			inline uint16_t family() { return ss_family; }

			inline void af_inet(uint16_t port, uint32_t addr = INADDR_ANY) {
				reset();
				auto& sa = cast<sockaddr_in>();
				sa.sin_family = AF_INET;
				sa.sin_port = htons(port);
				sa.sin_addr.s_addr = addr;
			}

			inline void af_inet6(uint16_t port, array<uint8_t, 16> addr = { 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 }, uint32_t flowinfo = 0, uint32_t scope_id = 0) {
				reset();
				auto& sa = cast<sockaddr_in6>();
				sa.sin6_family = AF_INET6;
				sa.sin6_port = htons(port);
				bcopy(addr.data(), &sa.sin6_addr, sizeof(sa.sin6_addr));
				sa.sin6_flowinfo = flowinfo;
				sa.sin6_scope_id = scope_id;
			}

			inline uint16_t port() const { return htons(cast<sockaddr_in>().sin_port); }
			inline uint32_t ip4() const { return htonl(cast<sockaddr_in>().sin_addr.s_addr); }
			inline const uint8_t* ip6() const { return  cast<sockaddr_in6>().sin6_addr.s6_addr; }

			inline struct sockaddr& sa() { return *reinterpret_cast<struct sockaddr*>(this); }
			inline struct sockaddr_in& sa_in() { return *reinterpret_cast<struct sockaddr_in *>(this); }
			inline struct sockaddr_in6& sa_in6() { return *reinterpret_cast<struct sockaddr_in6 *>(this); }
			inline struct sockaddr_storage& sa_ss() { return *reinterpret_cast<struct sockaddr_storage*>(this); }
		};

		class handle {
		protected:
			int						s_handle;
			int						s_errno;
			string					s_error;
			address					s_addr;
			struct pollfd			s_poll;
			inline int __getsockerror() {
				int error = 0;
				return (s_handle && getopt(SOL_SOCKET, SO_ERROR, error)) ? error : errno;
			}
			inline void __error(int code) {
				s_error = (s_errno = code) == 0 ? "" : strerror(s_errno);
#ifdef DEBUG
				code && fprintf(stderr, "SOCKET ERROR (%d): %s (code: %d)\n", s_handle, s_error.c_str(), s_errno);
#endif // DEBUG
				
			}
			inline void __error(int code, string&& err) { 
				s_errno = code; s_error = err;
#ifdef DEBUG
				code && fprintf(stderr, "SOCKET ERROR (%d): %s (code: %d)\n", s_handle, s_error.c_str(), s_errno);
#endif // DEBUG
				
			}
			handle() : s_handle(0), s_errno(0), s_error(), s_addr(), s_poll{0,0,0} { ; }
			virtual ~handle() { close(); }

			inline int accept(int& sock, address& addr) {
				socklen_t len = sizeof(sockaddr_storage);
				sock = ::accept(s_handle, &addr.sa(), &len);
				if (sock < 0) {
					if (errno == EWOULDBLOCK || errno == EAGAIN) {
						return 0;
					}
				}
				return sock;
			}
			
		public:

			inline const char* err_str() { return s_error.c_str(); }
			inline int err_no() { return s_errno; }

			inline int h() const { return s_handle; }
			address& addr() { return s_addr; }

			inline void attach(int sock, address&& addr = address()) {
				close();
				s_addr = move(addr);
				s_handle = sock;
				s_poll.fd = sock;
			}
			inline int detach(address&& addr = address()) {
				int sock = s_handle;
				s_addr.reset();
				s_handle = 0;
				s_errno = 0;
				s_error = "";
				s_poll.fd = 0;
				return sock;
			}

			inline bool getopt(int level, int optname, void* optval, size_t optlen) {
				return_ifnotsock(s_handle);
				return_ifsockerror(getsockopt(s_handle, level, optname, optval, (socklen_t*)&optlen));
				return true;
			}

			inline bool setopt(int level, int optname, const void* optval, size_t optlen) {
				return_ifnotsock(s_handle);
				return_ifsockerror(setsockopt(s_handle, level, optname, optval, (socklen_t)optlen));
				return true;
			}

			template <typename T>
			inline bool setopt(int level, int optname, T optval) {
				return setopt(level, optname, (const void*)&optval, sizeof(T));
			}

			template <typename T>
			inline bool getopt(int level, int optname, T& optval) {
				return getopt(level, optname, (void*)&optval, sizeof(T));
			}


			inline bool settimeout(double millisec) {
				struct timeval t = { (time_t)(floor((int)(millisec * 1000.)) / 1000000) , ((time_t)(millisec * 1000.)) % 1000000 };
				return setopt(SOL_SOCKET, SO_RCVTIMEO, t);
			}

			inline bool setreuseaddr(int enable) {
				return setopt(SOL_SOCKET, SO_REUSEADDR, enable);
			}

			inline void close() {
				auto sock = detach();
				if (sock > 0) {
					::close(sock);
				}
			}
			/* 
				return types of events that actually occurred.
				-1 - error
				0 - timeout exceed
				>0 revents
			*/
			inline int poll(size_t timeout_millisec, short events = POLLIN) {
				auto result = ::poll(&s_poll, 1, (int)timeout_millisec);
				if (result == 0 || s_poll.revents == 0 || (result == -1 && errno == EAGAIN) || (result == -1 && errno == EWOULDBLOCK)) {
					if (result < 0) { __error(errno); }
					return 0;
				}
				if (result == -1 || s_poll.revents != POLLIN) {
					__error(errno);
					return -1;
				}
				return s_poll.revents;
			}
		};

		

		static inline std::string iptostr(const uint32_t ip4a) {
			char ip_str_buffer[16];
			const uint8_t* ip_byte = (const uint8_t*)(&ip4a);
			ip_str_buffer[0] = '\0';
			snprintf(ip_str_buffer, 15, "%u.%u.%u.%u", ip_byte[3], ip_byte[2], ip_byte[1], ip_byte[0]);
			return ip_str_buffer;
		}
	}
}