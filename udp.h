#pragma once
#include "socket.h"
#include <functional>

namespace gear {
	namespace socket {
		namespace udp {
			using namespace std;
			class client : public handle {
			public:
				client() { ; }

				inline bool open(string device = string()) {
					close();
					if ((s_handle = ::socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) <= 0) {
						__error(errno);
						return false;
					}

					if (!device.empty() && !setopt(SOL_SOCKET, SO_BINDTODEVICE, device.c_str(), device.length())) {
						return false;
					}

					return true;
				}

				inline int recv(void* buffer, size_t length, int flags = 0) const {
					return (int)::recv(s_handle, buffer, length, flags);
				}

				inline int send(const void* buffer, size_t length, int flags = 0) {
					return (int)::sendto(s_handle, buffer, length, flags, (const sockaddr*)&s_addr.sa(), (socklen_t)sizeof(sockaddr_in));
				}
				inline int recvfrom(void* buffer, size_t length, address& from, int flags = 0) const {
					socklen_t len = (socklen_t)sizeof(sockaddr_storage);
					return (int)::recvfrom(s_handle, buffer, length, flags, &from.sa(), &len);
				}

				inline int sendto(const void* buffer, size_t length, uint16_t port_to, string ip_to = string(), int flags = 0) const {
					address	to;
					to.af_inet(port_to, ip_to.empty() ? INADDR_ANY : inet_addr(ip_to.c_str()));
					return (int)::sendto(s_handle, buffer, length, flags, &to.sa(), (socklen_t)sizeof(sockaddr_in));
				}
			};

			class server : public handle
			{
			protected:
				class events_list {
				protected:
					friend class server;
					function<void(client&&)> fn_receive;
					function<void()> fn_idle;
					events_list() { ; }

					inline void on_receive(client&& c) { if (fn_receive) { fn_receive(move(c)); } }
					inline void on_idle() { if (fn_idle) { fn_idle(); } }
				public:
					inline events_list& receive(function<void(client&&)>&& fn) { fn_receive = fn; return *this; }
					inline events_list& idle(function<void()>&& fn) { fn_idle = fn; return *this; }
				};

			public:
				events_list	events;

				server() { ; }
				~server() { ; }

				/*
				* Open tcp server
				*/
				inline bool open(uint16_t port, string ipaddr = string(), string device = string(), bool reuse_address = true) {
					close();
					if ((s_handle = ::socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) <= 0) {
						__error(errno);
						return false;
					}

					if (!device.empty() && !setopt(SOL_SOCKET, SO_BINDTODEVICE, device.c_str(), device.length())) {
						return false;
					}

					reuse_address && setreuseaddr(1);

					s_addr.af_inet(port, ipaddr.empty() ? INADDR_ANY : inet_addr(ipaddr.c_str()));

					if ((port || !ipaddr.empty()) && ::bind(s_handle, &s_addr.sa(), sizeof(sockaddr_in)) == -1) {
						__error(errno);
						return false;
					}
					return true;
				}

				/*
				* Listen client connection, send, recv, disconnect events
				*/
				inline bool listen(double timeout_millisec = 5000) {
					return_ifnotsock(s_handle);
					fd_set fd_read;
					struct timeval t = { (time_t)(floor((int)(timeout_millisec * 1000.)) / 1000000) , ((time_t)(timeout_millisec * 1000.)) % 1000000 };
					FD_ZERO(&fd_read);
					FD_SET(s_handle, &fd_read);

					switch (::select(s_handle + 1, &fd_read, nullptr, nullptr, &t)) {
					case -1:
						if (errno != EAGAIN) {
							__error(errno);
							return false;
						}
					case 0:
						events.on_idle();
						break;
					default:
					{
						if (FD_ISSET(s_handle, &fd_read)) {
							address		addr;
							socklen_t	len = sizeof(sockaddr_storage);

							int result = (int)::recvfrom(s_handle, nullptr, 0, MSG_PEEK, &addr.sa(), &len);

							if (result >= 0) {
								client sock;
								sock.attach(s_handle, move(addr));
								events.on_receive(move(sock));
								sock.detach();
							}
							else {
								__error(errno);
								return false;
							}
							return true;
						}
					}
					}
					return true;
				}
			};
		}
	}
}